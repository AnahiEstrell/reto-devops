from fastapi import FastAPI
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import logging
import os
from sqlalchemy.exc import SQLAlchemyError

# Configuración de la conexión a la base de datos
DATABASE_URL = os.getenv("DATABASE_URL")

# Configuración de logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Verificación de DATABASE_URL
if DATABASE_URL is None:
    raise ValueError("DATABASE_URL no está configurada.")

# Configuración de SQLAlchemy
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)

# Configuración de FastAPI
app = FastAPI()


# Definimos un endpoint en la ruta /saludo
@app.get("/saludo")
def obtener_saludo():
    # Intentamos realizar una conexión a la base de datos
    try:
        db = SessionLocal()
        return {"mensaje": "¡Hola! Conexión exitosa a la base de datos."}
    except SQLAlchemyError as e:
        logger.error(f"Error de conexión a la base de datos: {e}")
        return {"mensaje": "Error en la conexión a la base de datos."}
    finally:
        db.close()
