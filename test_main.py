from fastapi.testclient import TestClient
from main import app


def test_obtener_saludo(mocker):
    mocker.patch("main.SessionLocal")

    # Iniciar el cliente de prueba
    client = TestClient(app)

    # Realizar la solicitud al endpoint /saludo
    response = client.get("/saludo")

    # Verificar que la respuesta sea exitosa
    assert response.status_code == 200

    # Verificar el contenido de la respuesta
    expected_response = {"mensaje": "¡Hola! Conexión exitosa a la base de datos."}
    assert response.json() == expected_response
