# Reto - DevOps

Este proyecto implementa una API con un endpoint simple. A continuación, se describen algunos aspectos clave del proyecto:

## Dependencias

Las dependencias del proyecto están especificadas en el archivo `requirements.txt`. Estas se instalan automáticamente al ejecutar el contenedor.

## Comando para ejecutar el proyecto
```
$ docker-compose up
```
## Acceso al endpoint
Una vez que el contenedor está en ejecución, puedes acceder al siguiente endpoint:
```
$ http://localhost:8000/saludo
```
Al conectarte con esta ruta, estarás interactuando con una base de datos de PostgreSQL.

## Archivos Principales
El archivo principal del proyecto se encuentra en la carpeta raíz: `main.py`.
Las pruebas para el endpoint se encuentran en el archivo `test_main.py`.

## Archivos Ejemplo
Se proporcionan archivos de ejemplo que puedes modificar para ajustar algunos de los valores predeterminados del proyecto.

## Linter
El archivo de configuración del linter se encuentra en el archivo `.fake8`.
