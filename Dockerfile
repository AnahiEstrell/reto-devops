# Usa la imagen oficial de Python 3.9
FROM python:3.9

# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia el requirement.txt
COPY requirements.txt .

# Actualiza e instala las dependencias
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .

# Expone el puerto 8000
EXPOSE 8000

# Comando para ejecutar la aplicación al iniciar el contenedor
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]

